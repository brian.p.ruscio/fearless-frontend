
// import './App.css';
import Nav from './Nav';
import PresentationForm from './PresentationForm';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeConferenceForm from './AttendeeConference';
import MainPage from './MainPage'
import './logo.svg';
import { BrowserRouter, Routes, Route } from 'react-router-dom'


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="/conferences/new" element={<ConferenceForm />} />
            <Route path="/attendees/new" element={<AttendeeConferenceForm />} />
            <Route path="/locations/new" element={<LocationForm />} />
            <Route path="/presentation/new" element={<PresentationForm />} />
            <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
    </BrowserRouter>
  );
}

export default App;
