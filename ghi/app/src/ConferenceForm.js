import React, { useEffect, useState } from "react";

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [MaxPresentations, setMaxPresentations] = useState('');
    const [MaximumAttendees, setMaximumAttendees] = useState('');
    const [Location, setLocationChange] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSetStarts = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleSetEnds = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleChangeDescription =  (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleChangeMaxPresentations =  (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleChangeMaximumAttendees =  (event) => {
        const value = event.target.value;
        setMaximumAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocationChange(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();


        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = MaxPresentations;
        data.max_attendees = MaximumAttendees;
        data.location = Location;

        console.log(data);

        const conferenceURL = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceURL, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form id="create-conference-form" onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleSetStarts} placeholder="Starts" required type="date" value={starts} name="starts" id="starts" className="form-control"/>
                  <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleSetEnds} placeholder="Ends" required type="date" value={ends} name="ends" id="ends" className="form-control"/>
                  <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleChangeDescription} placeholder="Description" required type="text" value={description} name="description" id="description" className="form-control"></textarea>
                  <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                  <input  onChange={handleChangeMaxPresentations} placeholder="Max presentations" required type="number" value={MaxPresentations} name="max_presentations" id="max_presentations" className="form-control"/>
                  <label htmlFor="max_presentations">Max presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeMaximumAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" value={MaximumAttendees} id="max_attendees" className="form-control"/>
                  <label htmlFor="max_attendees">Max attendees</label>
                </div>
                <div className="mb-3">
                  <select required name="location" id="location" className="form-select" onChange={handleLocationChange} value={Location}>
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                          <option key={location.id} value={location.id}>
                              {location.name}
                          </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    </div>
    )
}

export default ConferenceForm
