import React, { useEffect, useState } from "react";


function AttendeeConferenceForm() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        console.log(value)
        setConference(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setConferences(data.conferences);
            console.log(data)
        }
    }


    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data = {};
        data.name = name;
        data.email = email;
        data.conference = conference;
        console.log(data)
        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeesUrl, fetchConfig);

            if(response.ok) {
                const newAttendee = await response.json();
                console.log(newAttendee);
                setName('');
                setEmail('');
                setConference('');
            }
    }



    useEffect(() => {
        fetchData();
    }, []);


    let spinnerClasses = 'd-flex justify-content-center mb-3'// what could go here?
    let dropdownClasses = 'form-select d-none'// what could go here?
    if (conferences.length > 0) {
        spinnerClasses = 'd-none d-flex justify-content-center mb-3'// what should be here?
        dropdownClasses = 'form-select' // what should be here?
    }

    return (
        <div className="my-5 container">
        <div className="my-5">
          <div className="row">
            <div className="col col-sm-auto">
              <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
            </div>
            <div className="col">
              <div className="card shadow">
                <div className="card-body">
                  <form onSubmit={handleSubmit} id="create-attendee-form">
                    <h1 className="card-title">It's Conference Time!</h1>
                    <p className="mb-3">
                      Please choose which conference
                      you'd like to attend.
                    </p>
                    <div className={spinnerClasses} id="loading-conference-spinner">
                      <div className="spinner-grow text-secondary" role="status">
                        <span className="visually-hidden">Loading...</span>
                      </div>
                    </div>
                    <div className="mb-3">
                      <select onChange={handleConferenceChange} name="conference" id="conference" className={dropdownClasses} required value={conference}>
                        <option value="">Choose a conference</option>
                        {conferences.map((conference) =>(
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                        ))}
                      </select>
                    </div>
                    <p className="mb-3">
                      Now, tell us about yourself.
                    </p>
                    <div className="row">
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" value={name}/>
                          <label htmlFor="name">Your full name</label>
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" value={email}/>
                          <label htmlFor="email">Your email address</label>
                        </div>
                      </div>
                    </div>
                    <button className="btn btn-lg btn-primary">I'm going!</button>
                  </form>
                  <div className="alert alert-success d-none mb-0" id="success-message">
                    Congratulations! You're all signed up!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}

export default AttendeeConferenceForm;
